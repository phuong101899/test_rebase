import React, { useEffect } from "react";

import logo from './logo.svg';
import './App.css';
import {Layout} from "./components/Layout";

function App() {
    useEffect(() => {
        console.log('Hook of Fleet: Conflict');
    }, [])
    return (
        <div className="App">
            <Layout>
                <header className="App-header">
                    <img src={logo} className="App-logo" alt="logo"/>
                    <p>
                        Edit <code>src/App.js</code> and save to reload.
                    </p>
                    <a
                        className="App-link"
                        href="https://reactjs.org"
                        target="_blank"
                        rel="noopener noreferrer"
                    >
                        Learn React
                    </a>
                </header>
            </Layout>

            <footer>
                Implement footer:1
            </footer>
        </div>
    );
}

export default App;
